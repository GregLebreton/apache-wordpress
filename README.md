# APACHE AVEC WORDPRESS #


## ENONCE:

    Le client veut une installation wordpress avec apache et mysql sur une machine.
    A partir d'une machine virtuelle crée à partir de l'image Debian 10, procéder à l'installation
    en notant le processus éffectué pour reproduire la procédure via script plus tard.
    Le but étant de se s'appuyer sur les installations déjà faite (wordpress, mysql) 
    et de trouver des exemples de configuration php pour apache afin de configurer celui-ci correctement.

        1) une VM debian 10
        2) installer wordpress 
        3) installer mariadb
        4) installer apache
        5) trouver comment configurer apache pour wordpress (google est ton ami)
        6) fournir un documents avec les étapes d'installation


## CORRECTION:

### 1) APACHE

        $ sudo apt install apache2

        $ sudo vim  /etc/apache2/sites-available/000-default.conf 

            DocumentRoot /var/www/html/wordpress
            <Directory /var/www/html/wordpress/>
            AllowOverride All
            </Directory>


### 2) MARIADB:

        $ sudo apt-get install software-properties-common dirmngr

        $ sudo apt-key adv --fetch-keys 'https://mariadb.org/mariadb_release_signing_key.asc'

        $ sudo add-apt-repository 'deb [arch=amd64,arm64,ppc64el] http://mariadb.mirrors.ovh.net/MariaDB/repo/10.5/debian buster main'

        $ sudo apt update

        $ sudo apt install mariadb-server mariadb-client -y

        $ sudo mysql

            CREATE DATABASE wpdb;
            CREATE USER 'wpuser'@'localhost' identified by 'dbpassword';
            GRANT ALL PRIVILEGES ON wpdb.* TO 'wpuser'@'localhost';
            FLUSH PRIVILEGES;
            exit


### 3) WORDPRESS:

        $ sudo apt install php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl   php-zip

        $ cd /var/www/html/

        $ sudo wget https://wordpress.org/latest.tar.gz

        $ sudo tar -xvzf latest.tar.gz

        $ cd wordpress

        $ cp wp-config-sample.php wp-config.php

        $ sudo nano wp-config.php

            // ** MySQL settings - You can get this info from your web host ** //
            /** The name of the database for WordPress */
            define( 'DB_NAME', 'wpdb' );

            /** MySQL database username */
            define( 'DB_USER', 'wpuser' );

            /** MySQL database password */
            define( 'DB_PASSWORD', 'dbpassword' );

            /** MySQL hostname */
            define( 'DB_HOST', 'localhost' );

        $ sudo chown -R www-data:www-data /var/www/html/wordpress


### 4) FNALISATION:

        $ sudo a2enmod rewrite

        $ sudo systemctl restart apache2
        

### 5) SECURISATION DU WORDPRESS:

        $ sudo curl -s https://api.wordpress.org/secret-key/1.1/salt/

        --> recopier la suite de clefs

        $ sudo nano /var/www/html/wordpress/wp-config.php

        --> remplacer les clefs exemple par celle fournit par la commande curl


